﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using FoodFinder.API.Model;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace FoodFinder.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment hostingEnvironment)
        {
            Configuration = configuration;
            Environment = hostingEnvironment;
        }

        public IConfiguration Configuration { get; }

        private IHostingEnvironment Environment { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContextPool<FoodFinderContext>(options =>
            {
                if (Environment.IsDevelopment())
                {
                    options.UseSqlite(Configuration.GetConnectionString("sqlite"));
                }
                else if (Environment.IsProduction())
                {
                    options.UseSqlServer(Configuration.GetConnectionString("MS_TableConnectionString"));
                }
                else
                {
                    options.UseInMemoryDatabase("inMem.db");
                }
            });

            if (Environment.IsProduction())
            {
                services.Configure<MvcOptions>(options =>
                {
                    options.Filters.Add(new RequireHttpsAttribute());
                });
            }

            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
                options.SerializerSettings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
            });



            // setup Swagger
            services.AddSwaggerGen(options =>
                {
                    options.AddSecurityDefinition("Bearer", new ApiKeyScheme()
                    {
                        In = "header",
                        Description = "Insert JWT api token, into Authorization field",
                        Name = "Authorization",
                        Type = "apiKey"
                    });

                    options.DescribeAllEnumsAsStrings();
                    options.DescribeAllParametersInCamelCase();
                    options.SwaggerDoc("v1", new Swashbuckle.AspNetCore.Swagger.Info
                    {
                        Title = "Food Finder API - Server",
                        Description = "Server implementation of Food Finder<br/>" +
                            "This is a template project,<br/> " +
                        "you can find the sourcecode <a href='https://friisconsult.com'>On bitbucket/github</a>",
                        Contact = new Contact
                        {
                            Email = "info.friisconsult.com",
                            Name = "Per Friis Consult aps & Friis Mobility ApS",
                            Url = "https://friisconsult.com"
                        },
                        TermsOfService = "Get the template from git, and start messing around.....",
                        Version = "1.0"
                    });

                    var filePath = PlatformServices.Default.Application.ApplicationBasePath + "FoodFinder.API.xml";
                    options.IncludeXmlComments(filePath);
                });

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.Authority = "https://friisconsult.eu.auth0.com/";
                options.Audience = "https://foodfinder.friisconsult.com/api/";
            });

        }



        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            
            app.UseAuthentication();
            
            app.UseSwagger();
            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "Walk The Talk - Server");
            });

            app.UseStaticFiles();

            app.UseMvc();
        }
    }
}
